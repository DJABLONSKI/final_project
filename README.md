# final_project

Final coding project.

Includes Environment file Final_Project_Jablonski.yml

Includes data for the final code:
Sounding data:
sounding2006.txt
sounding2010.txt
sounding2016.txt

Snowfall data:
11022006.csv
12052010.csv
12092016.csv

LakeSurface Temp data:
2006_306_glsea_sst.nc
2010_339_glsea_sst.nc
2016_343_glsea_sst.nc

Includes Final Code:
Final_Project_Code.ipynb
